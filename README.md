# Free-Music-Comparison

A comparison of free music streaming services

| | [Beatbump](https://github.com/snuffyDev/Beatbump) | [Hyperpipe](https://codeberg.org/Hyperpipe/Hyperpipe) | [Musi](https://feelthemusi.com) | [Spotify](https://open.spotify.com) | [Youtube Music](https://music.youtube.com) | [Brave Playlist](https://brave.com/playlist) |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| Platform | Web | Web | iOS | Desktop, Web, Mobile | Desktop, Web, Mobile | iOS |
| Open Source | ✅ | ✅ | ❌ | ❌ | ❌ | ✅ |
| Ad-Free | ✅ | ✅ | ❌ | ❌ | ❌ | ✅ |
| No ads during music | ✅ | ✅ | ✅ | ❌ | ❌ | ✅ |
| Downloads | ❌ | ❌ | ❌ | ❌ | ❌ | ✅ |
| DRM Free | ✅ | ✅ | ✅ | ❌ | ✅ | ✅ |
| Background play (mobile) | ✅ | ✅ | ✅ | ✅ | ❌ | ✅ |
| No account required | ✅ | ✅ | ✅ | ❌ | required on mobile | ✅ |
| Choose song | ✅ | ✅ | ✅ | ❌ | ✅ | ✅ |
| Search songs | ✅ | ✅ | ✅ | ✅ | ✅ | ❌ |
| Artist pages | ✅ | ✅ | ❌ | ✅ | ✅ | ❌ |
